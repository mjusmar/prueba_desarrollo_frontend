export interface UsuarioInfoI{
    personal_in :{
        nombre_completo:string;
        numero_id:string;
        celular:string;
        email:string;
        password:string;
    };
    residencia_in:{
        departamento: string;
        ciudad: string
        barrio: string
        direccion: string
    };
    financiero_in:{
        salario:string;
        otros_ingresos:string;
        gastos_mensuales:string;
        gastos_financieros:string;
    };

}