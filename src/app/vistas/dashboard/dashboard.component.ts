import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../servicios/api/api.service'
import {Router} from '@angular/router'
import { UsuarioInfoI } from 'src/app/modelos/usuarioinfo.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  usuario_info : UsuarioInfoI = <UsuarioInfoI>{};

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.checkLocalStorage();    
  }  
  
  checkLocalStorage(){
    if(localStorage.getItem('token')){
      this.api.getAll().subscribe(res =>{
        console.log(res);
        this.usuario_info = res;
      })      
    }
    else{
      this.router.navigate(['login']);
    }
  }
}
