import { Component, OnInit } from '@angular/core';

import {FormGroup, FormControl, Validator, Validators} from '@angular/forms'
import { ApiService } from '../../servicios/api/api.service'
import { LoginRequestI } from '../../modelos/request.interface'
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl('',Validators.required),
    password: new FormControl('',Validators.required)
  })
  
    constructor(private api: ApiService, private router:Router) {
   }

   errorStatus:boolean = false;
   errorMsj:any = "";

  ngOnInit(): void {
    // this.checkLocalStorage();
  }
  
  checkLocalStorage(){
    if(localStorage.getItem('token')){
      this.router.navigate(['dashboard']);
    }
  }

  onLogin(form:any){
    let login_form = {'personal_in': form}
    this.api.loginByEmail(login_form).subscribe(res => {
      console.log(res.token);
      console.log('entro');
      if(1){
        localStorage.setItem("token",res.token);
        this.router.navigate(['dashboard']);
      }
      else{
        console.log('msj');
        this.errorStatus = true;
        this.errorMsj = 'errorrrrr'
      }
    });
  }

  onTest(){
    let test = {"salario": 4000000 };
    this.api.getSalaryRange(test).subscribe(res => {
      console.log(res);
    });
  }

}
