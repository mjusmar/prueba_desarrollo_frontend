import { Injectable } from '@angular/core';
import {LoginRequestI, SalaryRequestI} from '../../modelos/request.interface';
import {LoginResponseI, SalaryResponseI} from '../../modelos/response.interface'
import { UsuarioInfoI } from 'src/app/modelos/usuarioinfo.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {Observable} from 'rxjs'



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  private readonly API = 'http://127.0.0.1:3333/api/v1/';
  headers: HttpHeaders = new HttpHeaders();
  

  constructor(private readonly http:HttpClient) { }

  getAll(): Observable <UsuarioInfoI> {    
    return this.http.get<UsuarioInfoI>(`${this.API}usuarios/getall`);
  }

  loginByEmail(form: LoginRequestI): Observable<LoginResponseI> {
    return this.http.post<LoginResponseI>(`${this.API}usuarios/login`,form);
  }  
  
  getSalaryRange(salario: SalaryRequestI): Observable<void>{
    var token ;
    token = (localStorage.getItem('token') != null) ? localStorage.getItem('token') : '';
    console.log('kk gordaaa', token)
    let header = this.headers.set('Authorization','Bearer '+token as string);
    return this.http.post<void>(`${this.API}salario/generar`,salario,{headers:header});
  }
}
